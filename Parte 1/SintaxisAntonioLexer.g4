lexer grammar SintaxisAntonioLexer;

//Espacios en blanco, comentarios, caracteres escape
COMMENTv3:      '/***' .*? '***/' -> skip;
COMMENTv2:      '/**' .*? '**/' -> skip;
COMMENTv1:      '/*' .*? '*/' -> skip;
COMMENT:        '//' .*? '\n' -> skip;
LINE_COMMENT:   '/'.? '*/' -> skip;
fragment ESC:   '\\'[btnr"\\];
WS:             [ \t\r\n]+ -> skip;

//Palabras reservadas
INCLUDE:    'include';
FUNCION:    'function';
BEGIN:      'begin';
END:        'end';
WHILE:      'while';
DEVOLVER:   'devolver';

//Tipos de datos
TIPO_DATO:  'cadena' | 'numero' | 'void';
CADENA:     ([a-zA-Z])+ ;
INT:        ([0-9])+;
TEXTO:      '"' (ESC | .)*? '"'; 

//Separadores
LPAREN:     '(';
RPAREN:     ')';
COMA:       ',';
PUNTO:      '.';
DOSPUNTOS:  ':';
fragment PUNTOYCOMA: ';';
ENDLINE:    PUNTOYCOMA;

//Operadores
ASIGNACION:     ':=';
COMPARACION:    '<' | '>' ;
CALCULO:        '+' | '-' ;