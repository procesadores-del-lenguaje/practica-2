// Generated from /home/roberto/Documentos/UAH/Procesadores Del Lenguaje/PL2/Parte 1/SintaxisAntonioLexer.g4 by ANTLR 4.7.1
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class SintaxisAntonioLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.7.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		COMMENTv3=1, COMMENTv2=2, COMMENTv1=3, COMMENT=4, LINE_COMMENT=5, WS=6, 
		INCLUDE=7, FUNCION=8, BEGIN=9, END=10, WHILE=11, DEVOLVER=12, TIPO_DATO=13, 
		CADENA=14, INT=15, TEXTO=16, LPAREN=17, RPAREN=18, COMA=19, PUNTO=20, 
		DOSPUNTOS=21, ENDLINE=22, ASIGNACION=23, COMPARACION=24, CALCULO=25;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] ruleNames = {
		"COMMENTv3", "COMMENTv2", "COMMENTv1", "COMMENT", "LINE_COMMENT", "ESC", 
		"WS", "INCLUDE", "FUNCION", "BEGIN", "END", "WHILE", "DEVOLVER", "TIPO_DATO", 
		"CADENA", "INT", "TEXTO", "LPAREN", "RPAREN", "COMA", "PUNTO", "DOSPUNTOS", 
		"PUNTOYCOMA", "ENDLINE", "ASIGNACION", "COMPARACION", "CALCULO"
	};

	private static final String[] _LITERAL_NAMES = {
		null, null, null, null, null, null, null, "'include'", "'function'", "'begin'", 
		"'end'", "'while'", "'devolver'", null, null, null, null, "'('", "')'", 
		"','", "'.'", "':'", null, "':='"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "COMMENTv3", "COMMENTv2", "COMMENTv1", "COMMENT", "LINE_COMMENT", 
		"WS", "INCLUDE", "FUNCION", "BEGIN", "END", "WHILE", "DEVOLVER", "TIPO_DATO", 
		"CADENA", "INT", "TEXTO", "LPAREN", "RPAREN", "COMA", "PUNTO", "DOSPUNTOS", 
		"ENDLINE", "ASIGNACION", "COMPARACION", "CALCULO"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public SintaxisAntonioLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "SintaxisAntonioLexer.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\33\u00ee\b\1\4\2"+
		"\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4"+
		"\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22"+
		"\t\22\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31"+
		"\t\31\4\32\t\32\4\33\t\33\4\34\t\34\3\2\3\2\3\2\3\2\3\2\3\2\7\2@\n\2\f"+
		"\2\16\2C\13\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3\7\3Q\n\3"+
		"\f\3\16\3T\13\3\3\3\3\3\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4\7\4`\n\4\f\4\16"+
		"\4c\13\4\3\4\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\7\5n\n\5\f\5\16\5q\13\5\3"+
		"\5\3\5\3\5\3\5\3\6\3\6\5\6y\n\6\3\6\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\b\6"+
		"\b\u0084\n\b\r\b\16\b\u0085\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3"+
		"\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\13\3\13\3\13\3\13\3\13\3\13\3\f\3"+
		"\f\3\f\3\f\3\r\3\r\3\r\3\r\3\r\3\r\3\16\3\16\3\16\3\16\3\16\3\16\3\16"+
		"\3\16\3\16\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17"+
		"\3\17\3\17\3\17\3\17\5\17\u00c4\n\17\3\20\6\20\u00c7\n\20\r\20\16\20\u00c8"+
		"\3\21\6\21\u00cc\n\21\r\21\16\21\u00cd\3\22\3\22\3\22\7\22\u00d3\n\22"+
		"\f\22\16\22\u00d6\13\22\3\22\3\22\3\23\3\23\3\24\3\24\3\25\3\25\3\26\3"+
		"\26\3\27\3\27\3\30\3\30\3\31\3\31\3\32\3\32\3\32\3\33\3\33\3\34\3\34\7"+
		"ARao\u00d4\2\35\3\3\5\4\7\5\t\6\13\7\r\2\17\b\21\t\23\n\25\13\27\f\31"+
		"\r\33\16\35\17\37\20!\21#\22%\23\'\24)\25+\26-\27/\2\61\30\63\31\65\32"+
		"\67\33\3\2\b\b\2$$^^ddppttvv\5\2\13\f\17\17\"\"\4\2C\\c|\3\2\62;\4\2>"+
		">@@\4\2--//\2\u00f7\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2"+
		"\13\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3"+
		"\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2"+
		"\2#\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2"+
		"\61\3\2\2\2\2\63\3\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2\39\3\2\2\2\5K\3\2\2"+
		"\2\7[\3\2\2\2\ti\3\2\2\2\13v\3\2\2\2\r\177\3\2\2\2\17\u0083\3\2\2\2\21"+
		"\u0089\3\2\2\2\23\u0091\3\2\2\2\25\u009a\3\2\2\2\27\u00a0\3\2\2\2\31\u00a4"+
		"\3\2\2\2\33\u00aa\3\2\2\2\35\u00c3\3\2\2\2\37\u00c6\3\2\2\2!\u00cb\3\2"+
		"\2\2#\u00cf\3\2\2\2%\u00d9\3\2\2\2\'\u00db\3\2\2\2)\u00dd\3\2\2\2+\u00df"+
		"\3\2\2\2-\u00e1\3\2\2\2/\u00e3\3\2\2\2\61\u00e5\3\2\2\2\63\u00e7\3\2\2"+
		"\2\65\u00ea\3\2\2\2\67\u00ec\3\2\2\29:\7\61\2\2:;\7,\2\2;<\7,\2\2<=\7"+
		",\2\2=A\3\2\2\2>@\13\2\2\2?>\3\2\2\2@C\3\2\2\2AB\3\2\2\2A?\3\2\2\2BD\3"+
		"\2\2\2CA\3\2\2\2DE\7,\2\2EF\7,\2\2FG\7,\2\2GH\7\61\2\2HI\3\2\2\2IJ\b\2"+
		"\2\2J\4\3\2\2\2KL\7\61\2\2LM\7,\2\2MN\7,\2\2NR\3\2\2\2OQ\13\2\2\2PO\3"+
		"\2\2\2QT\3\2\2\2RS\3\2\2\2RP\3\2\2\2SU\3\2\2\2TR\3\2\2\2UV\7,\2\2VW\7"+
		",\2\2WX\7\61\2\2XY\3\2\2\2YZ\b\3\2\2Z\6\3\2\2\2[\\\7\61\2\2\\]\7,\2\2"+
		"]a\3\2\2\2^`\13\2\2\2_^\3\2\2\2`c\3\2\2\2ab\3\2\2\2a_\3\2\2\2bd\3\2\2"+
		"\2ca\3\2\2\2de\7,\2\2ef\7\61\2\2fg\3\2\2\2gh\b\4\2\2h\b\3\2\2\2ij\7\61"+
		"\2\2jk\7\61\2\2ko\3\2\2\2ln\13\2\2\2ml\3\2\2\2nq\3\2\2\2op\3\2\2\2om\3"+
		"\2\2\2pr\3\2\2\2qo\3\2\2\2rs\7\f\2\2st\3\2\2\2tu\b\5\2\2u\n\3\2\2\2vx"+
		"\7\61\2\2wy\13\2\2\2xw\3\2\2\2xy\3\2\2\2yz\3\2\2\2z{\7,\2\2{|\7\61\2\2"+
		"|}\3\2\2\2}~\b\6\2\2~\f\3\2\2\2\177\u0080\7^\2\2\u0080\u0081\t\2\2\2\u0081"+
		"\16\3\2\2\2\u0082\u0084\t\3\2\2\u0083\u0082\3\2\2\2\u0084\u0085\3\2\2"+
		"\2\u0085\u0083\3\2\2\2\u0085\u0086\3\2\2\2\u0086\u0087\3\2\2\2\u0087\u0088"+
		"\b\b\2\2\u0088\20\3\2\2\2\u0089\u008a\7k\2\2\u008a\u008b\7p\2\2\u008b"+
		"\u008c\7e\2\2\u008c\u008d\7n\2\2\u008d\u008e\7w\2\2\u008e\u008f\7f\2\2"+
		"\u008f\u0090\7g\2\2\u0090\22\3\2\2\2\u0091\u0092\7h\2\2\u0092\u0093\7"+
		"w\2\2\u0093\u0094\7p\2\2\u0094\u0095\7e\2\2\u0095\u0096\7v\2\2\u0096\u0097"+
		"\7k\2\2\u0097\u0098\7q\2\2\u0098\u0099\7p\2\2\u0099\24\3\2\2\2\u009a\u009b"+
		"\7d\2\2\u009b\u009c\7g\2\2\u009c\u009d\7i\2\2\u009d\u009e\7k\2\2\u009e"+
		"\u009f\7p\2\2\u009f\26\3\2\2\2\u00a0\u00a1\7g\2\2\u00a1\u00a2\7p\2\2\u00a2"+
		"\u00a3\7f\2\2\u00a3\30\3\2\2\2\u00a4\u00a5\7y\2\2\u00a5\u00a6\7j\2\2\u00a6"+
		"\u00a7\7k\2\2\u00a7\u00a8\7n\2\2\u00a8\u00a9\7g\2\2\u00a9\32\3\2\2\2\u00aa"+
		"\u00ab\7f\2\2\u00ab\u00ac\7g\2\2\u00ac\u00ad\7x\2\2\u00ad\u00ae\7q\2\2"+
		"\u00ae\u00af\7n\2\2\u00af\u00b0\7x\2\2\u00b0\u00b1\7g\2\2\u00b1\u00b2"+
		"\7t\2\2\u00b2\34\3\2\2\2\u00b3\u00b4\7e\2\2\u00b4\u00b5\7c\2\2\u00b5\u00b6"+
		"\7f\2\2\u00b6\u00b7\7g\2\2\u00b7\u00b8\7p\2\2\u00b8\u00c4\7c\2\2\u00b9"+
		"\u00ba\7p\2\2\u00ba\u00bb\7w\2\2\u00bb\u00bc\7o\2\2\u00bc\u00bd\7g\2\2"+
		"\u00bd\u00be\7t\2\2\u00be\u00c4\7q\2\2\u00bf\u00c0\7x\2\2\u00c0\u00c1"+
		"\7q\2\2\u00c1\u00c2\7k\2\2\u00c2\u00c4\7f\2\2\u00c3\u00b3\3\2\2\2\u00c3"+
		"\u00b9\3\2\2\2\u00c3\u00bf\3\2\2\2\u00c4\36\3\2\2\2\u00c5\u00c7\t\4\2"+
		"\2\u00c6\u00c5\3\2\2\2\u00c7\u00c8\3\2\2\2\u00c8\u00c6\3\2\2\2\u00c8\u00c9"+
		"\3\2\2\2\u00c9 \3\2\2\2\u00ca\u00cc\t\5\2\2\u00cb\u00ca\3\2\2\2\u00cc"+
		"\u00cd\3\2\2\2\u00cd\u00cb\3\2\2\2\u00cd\u00ce\3\2\2\2\u00ce\"\3\2\2\2"+
		"\u00cf\u00d4\7$\2\2\u00d0\u00d3\5\r\7\2\u00d1\u00d3\13\2\2\2\u00d2\u00d0"+
		"\3\2\2\2\u00d2\u00d1\3\2\2\2\u00d3\u00d6\3\2\2\2\u00d4\u00d5\3\2\2\2\u00d4"+
		"\u00d2\3\2\2\2\u00d5\u00d7\3\2\2\2\u00d6\u00d4\3\2\2\2\u00d7\u00d8\7$"+
		"\2\2\u00d8$\3\2\2\2\u00d9\u00da\7*\2\2\u00da&\3\2\2\2\u00db\u00dc\7+\2"+
		"\2\u00dc(\3\2\2\2\u00dd\u00de\7.\2\2\u00de*\3\2\2\2\u00df\u00e0\7\60\2"+
		"\2\u00e0,\3\2\2\2\u00e1\u00e2\7<\2\2\u00e2.\3\2\2\2\u00e3\u00e4\7=\2\2"+
		"\u00e4\60\3\2\2\2\u00e5\u00e6\5/\30\2\u00e6\62\3\2\2\2\u00e7\u00e8\7<"+
		"\2\2\u00e8\u00e9\7?\2\2\u00e9\64\3\2\2\2\u00ea\u00eb\t\6\2\2\u00eb\66"+
		"\3\2\2\2\u00ec\u00ed\t\7\2\2\u00ed8\3\2\2\2\16\2ARaox\u0085\u00c3\u00c8"+
		"\u00cd\u00d2\u00d4\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}