// Generated from /home/roberto/Documentos/UAH/Procesadores Del Lenguaje/PL2/davis.g4 by ANTLR 4.7.1
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class davisParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		INCLUDE=1, STRING=2, ENDLINE=3, FUNCION=4, OPEN_PAR=5, CLOSE_PAR=6, DOUBLE_DOT=7, 
		TIPO=8, BEGIN=9, END=10, MIENTRAS=11, ASIGNAR=12, INT=13, TEXTO=14, ARITMETICO=15, 
		COMPARACION=16, LOGICO=17, COMA=18, RETURN=19;
	public static final int
		RULE_prog = 0, RULE_libreria = 1, RULE_funcion = 2, RULE_bucle = 3, RULE_asignacion = 4, 
		RULE_expresion = 5, RULE_llamada_funcion = 6, RULE_parametro = 7, RULE_contenido = 8, 
		RULE_linea = 9, RULE_retorno = 10;
	public static final String[] ruleNames = {
		"prog", "libreria", "funcion", "bucle", "asignacion", "expresion", "llamada_funcion", 
		"parametro", "contenido", "linea", "retorno"
	};

	private static final String[] _LITERAL_NAMES = {
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "INCLUDE", "STRING", "ENDLINE", "FUNCION", "OPEN_PAR", "CLOSE_PAR", 
		"DOUBLE_DOT", "TIPO", "BEGIN", "END", "MIENTRAS", "ASIGNAR", "INT", "TEXTO", 
		"ARITMETICO", "COMPARACION", "LOGICO", "COMA", "RETURN"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "davis.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public davisParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ProgContext extends ParserRuleContext {
		public List<LibreriaContext> libreria() {
			return getRuleContexts(LibreriaContext.class);
		}
		public LibreriaContext libreria(int i) {
			return getRuleContext(LibreriaContext.class,i);
		}
		public List<FuncionContext> funcion() {
			return getRuleContexts(FuncionContext.class);
		}
		public FuncionContext funcion(int i) {
			return getRuleContext(FuncionContext.class,i);
		}
		public ProgContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_prog; }
	}

	public final ProgContext prog() throws RecognitionException {
		ProgContext _localctx = new ProgContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_prog);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(25);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==INCLUDE) {
				{
				{
				setState(22);
				libreria();
				}
				}
				setState(27);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(31);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==FUNCION) {
				{
				{
				setState(28);
				funcion();
				}
				}
				setState(33);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LibreriaContext extends ParserRuleContext {
		public TerminalNode INCLUDE() { return getToken(davisParser.INCLUDE, 0); }
		public TerminalNode STRING() { return getToken(davisParser.STRING, 0); }
		public TerminalNode ENDLINE() { return getToken(davisParser.ENDLINE, 0); }
		public LibreriaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_libreria; }
	}

	public final LibreriaContext libreria() throws RecognitionException {
		LibreriaContext _localctx = new LibreriaContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_libreria);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(34);
			match(INCLUDE);
			setState(35);
			match(STRING);
			setState(36);
			match(ENDLINE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FuncionContext extends ParserRuleContext {
		public TerminalNode FUNCION() { return getToken(davisParser.FUNCION, 0); }
		public TerminalNode STRING() { return getToken(davisParser.STRING, 0); }
		public TerminalNode OPEN_PAR() { return getToken(davisParser.OPEN_PAR, 0); }
		public TerminalNode CLOSE_PAR() { return getToken(davisParser.CLOSE_PAR, 0); }
		public TerminalNode DOUBLE_DOT() { return getToken(davisParser.DOUBLE_DOT, 0); }
		public TerminalNode TIPO() { return getToken(davisParser.TIPO, 0); }
		public TerminalNode BEGIN() { return getToken(davisParser.BEGIN, 0); }
		public ContenidoContext contenido() {
			return getRuleContext(ContenidoContext.class,0);
		}
		public TerminalNode END() { return getToken(davisParser.END, 0); }
		public ParametroContext parametro() {
			return getRuleContext(ParametroContext.class,0);
		}
		public FuncionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_funcion; }
	}

	public final FuncionContext funcion() throws RecognitionException {
		FuncionContext _localctx = new FuncionContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_funcion);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(38);
			match(FUNCION);
			setState(39);
			match(STRING);
			setState(40);
			match(OPEN_PAR);
			setState(42);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==TIPO) {
				{
				setState(41);
				parametro();
				}
			}

			setState(44);
			match(CLOSE_PAR);
			setState(45);
			match(DOUBLE_DOT);
			setState(46);
			match(TIPO);
			setState(47);
			match(BEGIN);
			setState(48);
			contenido();
			setState(49);
			match(END);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BucleContext extends ParserRuleContext {
		public TerminalNode MIENTRAS() { return getToken(davisParser.MIENTRAS, 0); }
		public TerminalNode OPEN_PAR() { return getToken(davisParser.OPEN_PAR, 0); }
		public ExpresionContext expresion() {
			return getRuleContext(ExpresionContext.class,0);
		}
		public TerminalNode CLOSE_PAR() { return getToken(davisParser.CLOSE_PAR, 0); }
		public TerminalNode BEGIN() { return getToken(davisParser.BEGIN, 0); }
		public ContenidoContext contenido() {
			return getRuleContext(ContenidoContext.class,0);
		}
		public TerminalNode END() { return getToken(davisParser.END, 0); }
		public BucleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bucle; }
	}

	public final BucleContext bucle() throws RecognitionException {
		BucleContext _localctx = new BucleContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_bucle);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(51);
			match(MIENTRAS);
			setState(52);
			match(OPEN_PAR);
			setState(53);
			expresion(0);
			setState(54);
			match(CLOSE_PAR);
			setState(55);
			match(BEGIN);
			setState(56);
			contenido();
			setState(57);
			match(END);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AsignacionContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(davisParser.STRING, 0); }
		public TerminalNode ASIGNAR() { return getToken(davisParser.ASIGNAR, 0); }
		public LineaContext linea() {
			return getRuleContext(LineaContext.class,0);
		}
		public TerminalNode TIPO() { return getToken(davisParser.TIPO, 0); }
		public AsignacionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_asignacion; }
	}

	public final AsignacionContext asignacion() throws RecognitionException {
		AsignacionContext _localctx = new AsignacionContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_asignacion);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(60);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==TIPO) {
				{
				setState(59);
				match(TIPO);
				}
			}

			setState(62);
			match(STRING);
			setState(63);
			match(ASIGNAR);
			setState(64);
			linea();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpresionContext extends ParserRuleContext {
		public TerminalNode OPEN_PAR() { return getToken(davisParser.OPEN_PAR, 0); }
		public List<ExpresionContext> expresion() {
			return getRuleContexts(ExpresionContext.class);
		}
		public ExpresionContext expresion(int i) {
			return getRuleContext(ExpresionContext.class,i);
		}
		public TerminalNode CLOSE_PAR() { return getToken(davisParser.CLOSE_PAR, 0); }
		public TerminalNode INT() { return getToken(davisParser.INT, 0); }
		public TerminalNode STRING() { return getToken(davisParser.STRING, 0); }
		public TerminalNode TEXTO() { return getToken(davisParser.TEXTO, 0); }
		public Llamada_funcionContext llamada_funcion() {
			return getRuleContext(Llamada_funcionContext.class,0);
		}
		public TerminalNode ARITMETICO() { return getToken(davisParser.ARITMETICO, 0); }
		public TerminalNode COMPARACION() { return getToken(davisParser.COMPARACION, 0); }
		public TerminalNode LOGICO() { return getToken(davisParser.LOGICO, 0); }
		public ExpresionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expresion; }
	}

	public final ExpresionContext expresion() throws RecognitionException {
		return expresion(0);
	}

	private ExpresionContext expresion(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExpresionContext _localctx = new ExpresionContext(_ctx, _parentState);
		ExpresionContext _prevctx = _localctx;
		int _startState = 10;
		enterRecursionRule(_localctx, 10, RULE_expresion, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(75);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,4,_ctx) ) {
			case 1:
				{
				setState(67);
				match(OPEN_PAR);
				setState(68);
				expresion(0);
				setState(69);
				match(CLOSE_PAR);
				}
				break;
			case 2:
				{
				setState(71);
				match(INT);
				}
				break;
			case 3:
				{
				setState(72);
				match(STRING);
				}
				break;
			case 4:
				{
				setState(73);
				match(TEXTO);
				}
				break;
			case 5:
				{
				setState(74);
				llamada_funcion();
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(82);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,5,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new ExpresionContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_expresion);
					setState(77);
					if (!(precpred(_ctx, 6))) throw new FailedPredicateException(this, "precpred(_ctx, 6)");
					setState(78);
					_la = _input.LA(1);
					if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ARITMETICO) | (1L << COMPARACION) | (1L << LOGICO))) != 0)) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					setState(79);
					expresion(7);
					}
					} 
				}
				setState(84);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,5,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Llamada_funcionContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(davisParser.STRING, 0); }
		public TerminalNode OPEN_PAR() { return getToken(davisParser.OPEN_PAR, 0); }
		public TerminalNode CLOSE_PAR() { return getToken(davisParser.CLOSE_PAR, 0); }
		public List<ExpresionContext> expresion() {
			return getRuleContexts(ExpresionContext.class);
		}
		public ExpresionContext expresion(int i) {
			return getRuleContext(ExpresionContext.class,i);
		}
		public List<TerminalNode> COMA() { return getTokens(davisParser.COMA); }
		public TerminalNode COMA(int i) {
			return getToken(davisParser.COMA, i);
		}
		public Llamada_funcionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_llamada_funcion; }
	}

	public final Llamada_funcionContext llamada_funcion() throws RecognitionException {
		Llamada_funcionContext _localctx = new Llamada_funcionContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_llamada_funcion);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(85);
			match(STRING);
			setState(86);
			match(OPEN_PAR);
			setState(95);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << STRING) | (1L << OPEN_PAR) | (1L << INT) | (1L << TEXTO))) != 0)) {
				{
				setState(87);
				expresion(0);
				setState(92);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMA) {
					{
					{
					setState(88);
					match(COMA);
					setState(89);
					expresion(0);
					}
					}
					setState(94);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(97);
			match(CLOSE_PAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParametroContext extends ParserRuleContext {
		public List<TerminalNode> TIPO() { return getTokens(davisParser.TIPO); }
		public TerminalNode TIPO(int i) {
			return getToken(davisParser.TIPO, i);
		}
		public List<TerminalNode> STRING() { return getTokens(davisParser.STRING); }
		public TerminalNode STRING(int i) {
			return getToken(davisParser.STRING, i);
		}
		public List<TerminalNode> COMA() { return getTokens(davisParser.COMA); }
		public TerminalNode COMA(int i) {
			return getToken(davisParser.COMA, i);
		}
		public ParametroContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parametro; }
	}

	public final ParametroContext parametro() throws RecognitionException {
		ParametroContext _localctx = new ParametroContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_parametro);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(99);
			match(TIPO);
			setState(100);
			match(STRING);
			setState(106);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMA) {
				{
				{
				setState(101);
				match(COMA);
				setState(102);
				match(TIPO);
				setState(103);
				match(STRING);
				}
				}
				setState(108);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ContenidoContext extends ParserRuleContext {
		public List<BucleContext> bucle() {
			return getRuleContexts(BucleContext.class);
		}
		public BucleContext bucle(int i) {
			return getRuleContext(BucleContext.class,i);
		}
		public List<AsignacionContext> asignacion() {
			return getRuleContexts(AsignacionContext.class);
		}
		public AsignacionContext asignacion(int i) {
			return getRuleContext(AsignacionContext.class,i);
		}
		public List<LineaContext> linea() {
			return getRuleContexts(LineaContext.class);
		}
		public LineaContext linea(int i) {
			return getRuleContext(LineaContext.class,i);
		}
		public List<RetornoContext> retorno() {
			return getRuleContexts(RetornoContext.class);
		}
		public RetornoContext retorno(int i) {
			return getRuleContext(RetornoContext.class,i);
		}
		public ContenidoContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_contenido; }
	}

	public final ContenidoContext contenido() throws RecognitionException {
		ContenidoContext _localctx = new ContenidoContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_contenido);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(115);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << STRING) | (1L << OPEN_PAR) | (1L << TIPO) | (1L << MIENTRAS) | (1L << INT) | (1L << TEXTO) | (1L << RETURN))) != 0)) {
				{
				setState(113);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,9,_ctx) ) {
				case 1:
					{
					setState(109);
					bucle();
					}
					break;
				case 2:
					{
					setState(110);
					asignacion();
					}
					break;
				case 3:
					{
					setState(111);
					linea();
					}
					break;
				case 4:
					{
					setState(112);
					retorno();
					}
					break;
				}
				}
				setState(117);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LineaContext extends ParserRuleContext {
		public ExpresionContext expresion() {
			return getRuleContext(ExpresionContext.class,0);
		}
		public TerminalNode ENDLINE() { return getToken(davisParser.ENDLINE, 0); }
		public LineaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_linea; }
	}

	public final LineaContext linea() throws RecognitionException {
		LineaContext _localctx = new LineaContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_linea);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(118);
			expresion(0);
			setState(119);
			match(ENDLINE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RetornoContext extends ParserRuleContext {
		public TerminalNode RETURN() { return getToken(davisParser.RETURN, 0); }
		public LineaContext linea() {
			return getRuleContext(LineaContext.class,0);
		}
		public RetornoContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_retorno; }
	}

	public final RetornoContext retorno() throws RecognitionException {
		RetornoContext _localctx = new RetornoContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_retorno);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(121);
			match(RETURN);
			setState(122);
			linea();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 5:
			return expresion_sempred((ExpresionContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean expresion_sempred(ExpresionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 6);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\25\177\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\3\2\7\2\32\n\2\f\2\16\2\35\13\2\3\2\7\2 \n\2\f\2\16\2#\13"+
		"\2\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4\5\4-\n\4\3\4\3\4\3\4\3\4\3\4\3\4\3"+
		"\4\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\6\5\6?\n\6\3\6\3\6\3\6\3\6\3\7\3"+
		"\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\5\7N\n\7\3\7\3\7\3\7\7\7S\n\7\f\7\16\7"+
		"V\13\7\3\b\3\b\3\b\3\b\3\b\7\b]\n\b\f\b\16\b`\13\b\5\bb\n\b\3\b\3\b\3"+
		"\t\3\t\3\t\3\t\3\t\7\tk\n\t\f\t\16\tn\13\t\3\n\3\n\3\n\3\n\7\nt\n\n\f"+
		"\n\16\nw\13\n\3\13\3\13\3\13\3\f\3\f\3\f\3\f\2\3\f\r\2\4\6\b\n\f\16\20"+
		"\22\24\26\2\3\3\2\21\23\2\u0083\2\33\3\2\2\2\4$\3\2\2\2\6(\3\2\2\2\b\65"+
		"\3\2\2\2\n>\3\2\2\2\fM\3\2\2\2\16W\3\2\2\2\20e\3\2\2\2\22u\3\2\2\2\24"+
		"x\3\2\2\2\26{\3\2\2\2\30\32\5\4\3\2\31\30\3\2\2\2\32\35\3\2\2\2\33\31"+
		"\3\2\2\2\33\34\3\2\2\2\34!\3\2\2\2\35\33\3\2\2\2\36 \5\6\4\2\37\36\3\2"+
		"\2\2 #\3\2\2\2!\37\3\2\2\2!\"\3\2\2\2\"\3\3\2\2\2#!\3\2\2\2$%\7\3\2\2"+
		"%&\7\4\2\2&\'\7\5\2\2\'\5\3\2\2\2()\7\6\2\2)*\7\4\2\2*,\7\7\2\2+-\5\20"+
		"\t\2,+\3\2\2\2,-\3\2\2\2-.\3\2\2\2./\7\b\2\2/\60\7\t\2\2\60\61\7\n\2\2"+
		"\61\62\7\13\2\2\62\63\5\22\n\2\63\64\7\f\2\2\64\7\3\2\2\2\65\66\7\r\2"+
		"\2\66\67\7\7\2\2\678\5\f\7\289\7\b\2\29:\7\13\2\2:;\5\22\n\2;<\7\f\2\2"+
		"<\t\3\2\2\2=?\7\n\2\2>=\3\2\2\2>?\3\2\2\2?@\3\2\2\2@A\7\4\2\2AB\7\16\2"+
		"\2BC\5\24\13\2C\13\3\2\2\2DE\b\7\1\2EF\7\7\2\2FG\5\f\7\2GH\7\b\2\2HN\3"+
		"\2\2\2IN\7\17\2\2JN\7\4\2\2KN\7\20\2\2LN\5\16\b\2MD\3\2\2\2MI\3\2\2\2"+
		"MJ\3\2\2\2MK\3\2\2\2ML\3\2\2\2NT\3\2\2\2OP\f\b\2\2PQ\t\2\2\2QS\5\f\7\t"+
		"RO\3\2\2\2SV\3\2\2\2TR\3\2\2\2TU\3\2\2\2U\r\3\2\2\2VT\3\2\2\2WX\7\4\2"+
		"\2Xa\7\7\2\2Y^\5\f\7\2Z[\7\24\2\2[]\5\f\7\2\\Z\3\2\2\2]`\3\2\2\2^\\\3"+
		"\2\2\2^_\3\2\2\2_b\3\2\2\2`^\3\2\2\2aY\3\2\2\2ab\3\2\2\2bc\3\2\2\2cd\7"+
		"\b\2\2d\17\3\2\2\2ef\7\n\2\2fl\7\4\2\2gh\7\24\2\2hi\7\n\2\2ik\7\4\2\2"+
		"jg\3\2\2\2kn\3\2\2\2lj\3\2\2\2lm\3\2\2\2m\21\3\2\2\2nl\3\2\2\2ot\5\b\5"+
		"\2pt\5\n\6\2qt\5\24\13\2rt\5\26\f\2so\3\2\2\2sp\3\2\2\2sq\3\2\2\2sr\3"+
		"\2\2\2tw\3\2\2\2us\3\2\2\2uv\3\2\2\2v\23\3\2\2\2wu\3\2\2\2xy\5\f\7\2y"+
		"z\7\5\2\2z\25\3\2\2\2{|\7\25\2\2|}\5\24\13\2}\27\3\2\2\2\r\33!,>MT^al"+
		"su";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}