parser grammar SintaxisAntonioParser;

options { tokenVocab=SintaxisAntonioLexer; }

prog: (biblioteca | funcion)* ;

biblioteca: INCLUDE CADENA ENDLINE;

funcion: FUNCION CADENA LPAREN parametro? RPAREN DOSPUNTOS TIPO_DATO
    BEGIN 
        cuerpo 
    END;

mientras: WHILE LPAREN expr RPAREN
    BEGIN 
        cuerpo 
    END;

expr:  
    expr (COMPARACION | CALCULO) expr   #comparacionoCalculo
    |   LPAREN expr RPAREN              #expresion
    |   llamada_funcion                 #llamadaFuncion
    |	INT                             #entero
    |   CADENA                          #cadena
    |   TEXTO	                        #texto
    ;

//Cuerpo de funcion
asignacion:         TIPO_DATO? CADENA ASIGNACION linea;
declaracion:        TIPO_DATO (CADENA | INT);
parametro:          TIPO_DATO CADENA (COMA TIPO_DATO CADENA)*;
cuerpo:             (mientras | asignacion | linea | devolver)*;
linea:              expr ENDLINE;
devolver:           DEVOLVER linea;
llamada_funcion:    CADENA LPAREN (expr (COMA expr)*)? RPAREN;  
