package Gramaticas;

import Javas.SintaxisAntonioLexer;
import Javas.SintaxisAntonioParser;
import org.antlr.v4.runtime.*;

import java.io.*;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

public class Ejecucion{

    public static void main (String[] args) throws Exception{

        String inputFile = null;
        if(args.length>0) inputFile = args[0];
        
        inputFile = "/home/roberto/Documentos/UAH/Procesadores Del Lenguaje/PL2/Parte 2 Netbeans/src/Programa/PL2-fibonacci.prog";
        
        InputStream is = System.in;
        if(inputFile!=null){
            is = new FileInputStream(inputFile);
        }
        CharStream input = CharStreams.fromStream(is);
        Javas.SintaxisAntonioLexer lexer = new SintaxisAntonioLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        Javas.SintaxisAntonioParser parser = new SintaxisAntonioParser(tokens);
        parser.setBuildParseTree(true);
        ParseTree tree = parser.prog();
        
        SintaxisAntonioListener_Basico TB = new SintaxisAntonioListener_Basico();
        ParseTreeWalker walker = new ParseTreeWalker();

        walker.walk(TB,tree);
        System.out.println(TB.datosFilaActual());

    }

}