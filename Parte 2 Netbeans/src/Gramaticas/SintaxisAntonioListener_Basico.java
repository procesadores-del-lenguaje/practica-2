/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gramaticas;

import Javas.SintaxisAntonioParser;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import java.util.*;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;


public class SintaxisAntonioListener_Basico extends Javas.SintaxisAntonioParserBaseListener{

    public static final String CAMPO_VACIO = "";

    private EstructuraDeDatos data = new EstructuraDeDatos();

    public SintaxisAntonioListener_Basico() { }
        
    public ArrayList<String> datosFilaActual(){
        return data.mostrarData();
    }
    
    @Override public void enterProg(SintaxisAntonioParser.ProgContext ctx) {
        data.addActual("prog");
    }
	
    @Override public void enterBiblioteca(SintaxisAntonioParser.BibliotecaContext ctx) { 
        data.addActual("include");
    }

    @Override public void enterFuncion(SintaxisAntonioParser.FuncionContext ctx) { 
        data.addActual("función");
    }

    @Override public void enterMientras(SintaxisAntonioParser.MientrasContext ctx) { 
        data.addActual("while");
    }

    @Override public void enterTexto(SintaxisAntonioParser.TextoContext ctx) { 
        data.addActual(ctx.TEXTO().getText());
    }

    @Override public void enterExpresion(SintaxisAntonioParser.ExpresionContext ctx) { 
        data.addActual("expr");
    }

    @Override public void enterLlamadaFuncion(SintaxisAntonioParser.LlamadaFuncionContext ctx) { 
        data.addActual("call_function");
    }

    @Override public void enterCadena(SintaxisAntonioParser.CadenaContext ctx) { 
        data.addActual(ctx.CADENA().getText());
    }

    @Override public void enterEntero(SintaxisAntonioParser.EnteroContext ctx) { 
        data.addActual("int:"+ctx.INT().getText());
    }

    @Override public void enterComparacionoCalculo(SintaxisAntonioParser.ComparacionoCalculoContext ctx) { 
        data.addActual("sentencia");
    }


    @Override public void enterAsignacion(SintaxisAntonioParser.AsignacionContext ctx) { 
        data.addActual("asignacion");
    }


    @Override public void enterDeclaracion(SintaxisAntonioParser.DeclaracionContext ctx) { 
        data.addActual("Declaración:" + ctx.TIPO_DATO().getText());
    }


    @Override public void enterParametro(SintaxisAntonioParser.ParametroContext ctx) { 
        data.addActual(ctx.TIPO_DATO(0).getText()+":"+ctx.CADENA(0).getText());
    }

    @Override public void enterCuerpo(SintaxisAntonioParser.CuerpoContext ctx) { 
        data.addActual("cuerpo_funcion");
    }

    @Override public void enterLinea(SintaxisAntonioParser.LineaContext ctx) { 
        data.addActual("linea");
    }

    @Override public void enterDevolver(SintaxisAntonioParser.DevolverContext ctx) { 
        data.addActual("return" + ":" + ctx.DEVOLVER());
    }

    @Override public void enterLlamada_funcion(SintaxisAntonioParser.Llamada_funcionContext ctx) { 
        data.addActual("llamada_funcion:"+ctx.CADENA().getText());
    }


    @Override public void enterEveryRule(ParserRuleContext ctx) { 
    
    }
    
    @Override public void exitEveryRule(ParserRuleContext ctx) { 
        data.removeActual();
    }

    @Override public void visitTerminal(TerminalNode node) { 
        super.visitTerminal(node);
    }

    @Override public void visitErrorNode(ErrorNode node) { }
}