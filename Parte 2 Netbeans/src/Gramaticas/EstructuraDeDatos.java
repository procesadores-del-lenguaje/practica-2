package Gramaticas;

import java.io.BufferedWriter;
import java.io.File;
import java.util.ArrayList;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EstructuraDeDatos {
    
    private ArrayList<String> actual = new ArrayList<String>();
    private BufferedWriter bw = null;
    private FileWriter fw = null;
    
    
    public EstructuraDeDatos(){ }

    public void addActual(String cadena){
        actual.add(cadena);
        try {
            printActual();
        } catch (IOException ex) {
            Logger.getLogger(EstructuraDeDatos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void removeActual(){
        actual.remove(actual.size()-1);
    }
    
    public ArrayList<String> mostrarData(){
        return actual;
    } 
    
    public void printActual() throws IOException{
        String imprimir = "";
        for (int i = 0; i < actual.size(); i++) {
            imprimir = imprimir + "/" + actual.get(i);
        }
        System.out.println(imprimir);
        String file = "/home/roberto/Documentos/UAH/Procesadores Del Lenguaje/PL2/Parte 2 Netbeans/Salida.txt" +"\n";
        agregarTextoAlfinal(file, imprimir);
    }
    
    public void agregarTextoAlfinal(String nombreArch, String texto){
        try {
            FileWriter fstream = new FileWriter(nombreArch, true);
            BufferedWriter out = new BufferedWriter(fstream);
            out.write(texto+"\n");
            out.close();
        } catch (IOException ex) {
            System.out.println("Error: "+ex.getMessage());
        }
 }

}
