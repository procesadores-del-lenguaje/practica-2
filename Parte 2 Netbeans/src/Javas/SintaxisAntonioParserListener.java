package Javas;

// Generated from SintaxisAntonioParser.g4 by ANTLR 4.7.1
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link SintaxisAntonioParser}.
 */
public interface SintaxisAntonioParserListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link SintaxisAntonioParser#prog}.
	 * @param ctx the parse tree
	 */
	void enterProg(SintaxisAntonioParser.ProgContext ctx);
	/**
	 * Exit a parse tree produced by {@link SintaxisAntonioParser#prog}.
	 * @param ctx the parse tree
	 */
	void exitProg(SintaxisAntonioParser.ProgContext ctx);
	/**
	 * Enter a parse tree produced by {@link SintaxisAntonioParser#biblioteca}.
	 * @param ctx the parse tree
	 */
	void enterBiblioteca(SintaxisAntonioParser.BibliotecaContext ctx);
	/**
	 * Exit a parse tree produced by {@link SintaxisAntonioParser#biblioteca}.
	 * @param ctx the parse tree
	 */
	void exitBiblioteca(SintaxisAntonioParser.BibliotecaContext ctx);
	/**
	 * Enter a parse tree produced by {@link SintaxisAntonioParser#funcion}.
	 * @param ctx the parse tree
	 */
	void enterFuncion(SintaxisAntonioParser.FuncionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SintaxisAntonioParser#funcion}.
	 * @param ctx the parse tree
	 */
	void exitFuncion(SintaxisAntonioParser.FuncionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SintaxisAntonioParser#mientras}.
	 * @param ctx the parse tree
	 */
	void enterMientras(SintaxisAntonioParser.MientrasContext ctx);
	/**
	 * Exit a parse tree produced by {@link SintaxisAntonioParser#mientras}.
	 * @param ctx the parse tree
	 */
	void exitMientras(SintaxisAntonioParser.MientrasContext ctx);
	/**
	 * Enter a parse tree produced by the {@code texto}
	 * labeled alternative in {@link SintaxisAntonioParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterTexto(SintaxisAntonioParser.TextoContext ctx);
	/**
	 * Exit a parse tree produced by the {@code texto}
	 * labeled alternative in {@link SintaxisAntonioParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitTexto(SintaxisAntonioParser.TextoContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expresion}
	 * labeled alternative in {@link SintaxisAntonioParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpresion(SintaxisAntonioParser.ExpresionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expresion}
	 * labeled alternative in {@link SintaxisAntonioParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpresion(SintaxisAntonioParser.ExpresionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code llamadaFuncion}
	 * labeled alternative in {@link SintaxisAntonioParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterLlamadaFuncion(SintaxisAntonioParser.LlamadaFuncionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code llamadaFuncion}
	 * labeled alternative in {@link SintaxisAntonioParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitLlamadaFuncion(SintaxisAntonioParser.LlamadaFuncionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code cadena}
	 * labeled alternative in {@link SintaxisAntonioParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterCadena(SintaxisAntonioParser.CadenaContext ctx);
	/**
	 * Exit a parse tree produced by the {@code cadena}
	 * labeled alternative in {@link SintaxisAntonioParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitCadena(SintaxisAntonioParser.CadenaContext ctx);
	/**
	 * Enter a parse tree produced by the {@code entero}
	 * labeled alternative in {@link SintaxisAntonioParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterEntero(SintaxisAntonioParser.EnteroContext ctx);
	/**
	 * Exit a parse tree produced by the {@code entero}
	 * labeled alternative in {@link SintaxisAntonioParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitEntero(SintaxisAntonioParser.EnteroContext ctx);
	/**
	 * Enter a parse tree produced by the {@code comparacionoCalculo}
	 * labeled alternative in {@link SintaxisAntonioParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterComparacionoCalculo(SintaxisAntonioParser.ComparacionoCalculoContext ctx);
	/**
	 * Exit a parse tree produced by the {@code comparacionoCalculo}
	 * labeled alternative in {@link SintaxisAntonioParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitComparacionoCalculo(SintaxisAntonioParser.ComparacionoCalculoContext ctx);
	/**
	 * Enter a parse tree produced by {@link SintaxisAntonioParser#asignacion}.
	 * @param ctx the parse tree
	 */
	void enterAsignacion(SintaxisAntonioParser.AsignacionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SintaxisAntonioParser#asignacion}.
	 * @param ctx the parse tree
	 */
	void exitAsignacion(SintaxisAntonioParser.AsignacionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SintaxisAntonioParser#declaracion}.
	 * @param ctx the parse tree
	 */
	void enterDeclaracion(SintaxisAntonioParser.DeclaracionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SintaxisAntonioParser#declaracion}.
	 * @param ctx the parse tree
	 */
	void exitDeclaracion(SintaxisAntonioParser.DeclaracionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SintaxisAntonioParser#parametro}.
	 * @param ctx the parse tree
	 */
	void enterParametro(SintaxisAntonioParser.ParametroContext ctx);
	/**
	 * Exit a parse tree produced by {@link SintaxisAntonioParser#parametro}.
	 * @param ctx the parse tree
	 */
	void exitParametro(SintaxisAntonioParser.ParametroContext ctx);
	/**
	 * Enter a parse tree produced by {@link SintaxisAntonioParser#cuerpo}.
	 * @param ctx the parse tree
	 */
	void enterCuerpo(SintaxisAntonioParser.CuerpoContext ctx);
	/**
	 * Exit a parse tree produced by {@link SintaxisAntonioParser#cuerpo}.
	 * @param ctx the parse tree
	 */
	void exitCuerpo(SintaxisAntonioParser.CuerpoContext ctx);
	/**
	 * Enter a parse tree produced by {@link SintaxisAntonioParser#linea}.
	 * @param ctx the parse tree
	 */
	void enterLinea(SintaxisAntonioParser.LineaContext ctx);
	/**
	 * Exit a parse tree produced by {@link SintaxisAntonioParser#linea}.
	 * @param ctx the parse tree
	 */
	void exitLinea(SintaxisAntonioParser.LineaContext ctx);
	/**
	 * Enter a parse tree produced by {@link SintaxisAntonioParser#devolver}.
	 * @param ctx the parse tree
	 */
	void enterDevolver(SintaxisAntonioParser.DevolverContext ctx);
	/**
	 * Exit a parse tree produced by {@link SintaxisAntonioParser#devolver}.
	 * @param ctx the parse tree
	 */
	void exitDevolver(SintaxisAntonioParser.DevolverContext ctx);
	/**
	 * Enter a parse tree produced by {@link SintaxisAntonioParser#llamada_funcion}.
	 * @param ctx the parse tree
	 */
	void enterLlamada_funcion(SintaxisAntonioParser.Llamada_funcionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SintaxisAntonioParser#llamada_funcion}.
	 * @param ctx the parse tree
	 */
	void exitLlamada_funcion(SintaxisAntonioParser.Llamada_funcionContext ctx);
}